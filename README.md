# IMAPSiteSync

Save an IMAP draft folder to a local folder in a text or html format with attachments.

## Install

This script requires Python 3 and the following libraries:
* [six](https://pypi.org/project/six)
* [chardet](https://pypi.python.org/pypi/chardet) – required for character encoding detection.

## Use cases

* Maintain and update a website by editing files within the draft folder of an IMAP account like gmail.
* The subject line is used as the files name.
* Add delete to the end of the subject to delete the file from the server.
  * This will also delete all attached files.
* In order to update a file set the draft email to be marked as unread.  After it is updated it will be marked read on the server.
* You will need to setup a cron job to call the imapsitesync.py script to keep the site up to date with the the mail files.
* If the email is set to plain text format it will use the exact text of the email.  It the format is HTML then it will save the HTML code into the file on the server instead.
* If the subject ends with a / and had a blank body then the attachments will be saved within the folder.  For example, if the subject is "content/product1/images/" and there are attachements then the content/product1/images/ structure will be created and the attachments will be saved into this folder.


## Config file

Use `~/.config/imapsitesync/config.cfg` or `/etc/imapsitesync/config.cfg`

Example:
```ini
[imapsitesync]
local_folder=/var/www/html/pico/

[account1]
host=mail.autistici.org
username=username@domain
password=secret

[account2]
host=imap.googlemail.com
username=username@gmail.com
password=secret
remote_folder="[Gmail]/Drafts"
port=993
```



The imapsitesync section
-------------------

Possibles parameters for the imapsitesync section:

Parameter       | Description
----------------|----------------------
local_folder    | The full path to the folder where the emails should be stored. If the local_folder is not set, imapsitesync will download the emails in the current directory. This can be overwritten with the shell argument `-l`.



Other sections
--------------

You can have has many configured account as you want, one per section. Sections names may contains the account name.

Possibles parameters for an account section:

Parameter       | Description
----------------|----------------------
host            | IMAP server hostname
username        | Login id for the IMAP server.
password        | The password will be saved in cleartext, for security reasons, you have to run the imapsitesync script in userspace and set `chmod 700` on your `~/.config/imapsitesync/config.cfg` file.
remote_folder   | (optional) IMAP folder name (multiple folder name is not supported for the moment). Default value is `"[Gmail]/Drafts"`.
port            | (optional) Default value is `993`.


## License

The MIT License (MIT)
