#!/usr/bin/env python
#-*- coding:utf-8 -*-

from __future__ import print_function

import imaplib, email
import re
import os
import hashlib
from message import Message
import datetime


class MailboxClient:
    """Operations on a mailbox"""

    def __init__(self, host, port, username, password, remote_folder):
        self.mailbox = imaplib.IMAP4_SSL(host, port)
        self.mailbox.login(username, password)
        self.mailbox.select(remote_folder, readonly=False)

    def copy_emails(self, local_folder):

        n_saved = 0
        n_exists = 0

        self.local_folder = local_folder

        typ, data = self.mailbox.search(None, '(UNSEEN)')
        for num in data[0].split():
            typ, data = self.mailbox.fetch(num, '(RFC822)')
            if self.saveEmail(data):
                n_saved += 1
            else:
                n_exists += 1

        return (n_saved, n_exists)


    def cleanup(self):
        self.mailbox.close()
        self.mailbox.logout()


    def getEmailFilename(self, msg, data):
        if msg['Subject']:
            foldername = re.sub('[^\\a-zA-Z0-9_\-\.()\s]+', '', msg['Subject'])
            print('subject2=' + foldername)
        else:
            foldername = hashlib.sha224(data).hexdigest()
        return os.path.join(self.local_folder, foldername)



    def saveEmail(self, data):
        for response_part in data:
            if isinstance(response_part, tuple):
                msg = email.message_from_string(response_part[1].decode("utf-8"))
                emailfilepath = self.getEmailFilename(msg, data[0][1])
                print('file ' + emailfilepath)

                if os.path.isfile(emailfilepath):
                    print("deleting " + emailfilepath)
                    os.remove(emailfilepath)

                #try:
                message = Message(emailfilepath, msg)
                message.saveEmailPartsAsFiles()

                #except Exception as e:
                #    # ex: Unsupported charset on decode
                #    print(emailfilepath)
                #    if hasattr(e, 'strerror'):
                #        print("MailboxClient.saveEmail() failed:", e.strerror)
                #    else:
                #        print("MailboxClient.saveEmail() failed")
                #        print(e)

        return True
