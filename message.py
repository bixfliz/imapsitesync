#!/usr/bin/env python
#-*- coding:utf-8 -*-


import email
from email.utils import parseaddr
from email.header import decode_header
import re
import os
import subprocess
import unicodedata
import json
import io
import mimetypes
import chardet
import gzip
import cgi
import time
import pkgutil

from six.moves import html_parser


# email address REGEX matching the RFC 2822 spec
# from perlfaq9
#    my $atom       = qr{[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+};
#    my $dot_atom   = qr{$atom(?:\.$atom)*};
#    my $quoted     = qr{"(?:\\[^\r\n]|[^\\"])*"};
#    my $local      = qr{(?:$dot_atom|$quoted)};
#    my $domain_lit = qr{\[(?:\\\S|[\x21-\x5a\x5e-\x7e])*\]};
#    my $domain     = qr{(?:$dot_atom|$domain_lit)};
#    my $addr_spec  = qr{$local\@$domain};
#
# Python translation

atom_rfc2822=r"[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+"
atom_posfix_restricted=r"[a-zA-Z0-9_#\$&'*+/=?\^`{}~|\-]+" # without '!' and '%'
atom=atom_rfc2822
dot_atom=atom  +  r"(?:\."  +  atom  +  ")*"
quoted=r'"(?:\\[^\r\n]|[^\\"])*"'
local="(?:"  +  dot_atom  +  "|"  +  quoted  +  ")"
domain_lit=r"\[(?:\\\S|[\x21-\x5a\x5e-\x7e])*\]"
domain="(?:"  +  dot_atom  +  "|"  +  domain_lit  +  ")"
addr_spec=local  +  "\@"  +  domain

email_address_re=re.compile('^'+addr_spec+'$')




class MLStripper(html_parser.HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def convert_charrefs(x):
        return x
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()



class Message:
    """Operation on a message"""

    def __init__(self, emailFilepath, msg):
        self.msg = msg
        self.emailFilepath = emailFilepath

    def getmailheader(self, header_text, default="ascii"):
        """Decode header_text if needed"""
        try:
            headers=decode_header(header_text)
        except email.Errors.HeaderParseError:
            # This already append in email.base64mime.decode()
            # instead return a sanitized ascii string
            return header_text.encode('ascii', 'replace').decode('ascii')
        else:
            for i, (text, charset) in enumerate(headers):
                headers[i]=text
                if charset:
                    headers[i]=str(text, charset)
                else:
                    headers[i]=str(text)
            return u"".join(headers)


    def getmailaddresses(self, prop):
        """retrieve From:, To: and Cc: addresses"""
        addrs=email.utils.getaddresses(self.msg.get_all(prop, []))
        for i, (name, addr) in enumerate(addrs):
            if not name and addr:
                # only one string! Is it the address or is it the name ?
                # use the same for both and see later
                name=addr

            try:
                # address must be ascii only
                addr=addr.encode('ascii')
            except UnicodeError:
                addr=''
            else:
                # address must match adress regex
                if not email_address_re.match(addr.decode("utf-8")):
                    addr=''
            addrs[i]=(self.getmailheader(name), addr.decode("utf-8"))
        return addrs

    def getSubject(self):
        if not hasattr(self, 'subject'):
            self.subject = self.getmailheader(self.msg.get('Subject', ''))
        print('subject=' + self.subject)
        return self.subject

    def getFrom(self):
        if not hasattr(self, 'from_'):
            self.from_ = self.getmailaddresses('from')
            self.from_ = ('', '') if not self.from_ else self.from_[0]
        return self.from_

    def normalizeDate(self, datestr):
        t = email.utils.parsedate_tz(datestr)
        timeval = time.mktime(t[:-1])
        date = email.utils.formatdate(timeval, True)
        utc = time.gmtime(email.utils.mktime_tz(t))
        rfc2822 = '{} {:+03d}00'.format(date[:-6], t[9]//3600)
        iso8601 = time.strftime('%Y%m%dT%H%M%SZ', utc)

        return (rfc2822, iso8601)


    def getPartCharset(self, part):
        if part.get_content_charset() is None:
            return chardet.detect(str(part))['encoding']
        return part.get_content_charset()


    def getTextContent(self, parts):
        if not hasattr(self, 'text_content'):
            self.text_content = ''
            for part in parts:
                raw_content = part.get_payload(decode=True)
                charset = self.getPartCharset(part)
                self.text_content += raw_content.decode(charset, "replace")
        return self.text_content


    def createTextFile(self, parts):
        utf8_content = self.getTextContent(parts)
        if len(utf8_content) < 1:
            return
        if self.emailFilepath.endswith(' delete'):
            filetodelete = self.emailFilepath.replace(' delete', '')
            if os.path.isfile(filetodelete):
                os.remove(filetodelete)
                print('deleted file ' + filetodelete)
                return
        else:
            commandToRun = ''
            if utf8_content.startswith('imapsitesync-command: '):
                searchObj = re.search(r'^imapsitesync-command: (.*)\n(.*)', utf8_content, re.I)
                commandToRun = searchObj.group(1)
                commandToRun = commandToRun.strip()
                emailWithoutCommandLine = utf8_content.replace('imapsitesync-command: ' + commandToRun,'',1)
                if len(commandToRun) > 0:
                    utf8_content = emailWithoutCommandLine
            if not os.path.exists(os.path.dirname(self.emailFilepath)):
                os.makedirs(os.path.dirname(self.emailFilepath))
                print('created path ' + os.path.dirname(self.emailFilepath))
            if self.emailFilepath.endswith('/'):
                return
            with open(os.path.join(self.emailFilepath), 'wb') as fp:
                fp.write(bytearray(utf8_content, 'utf-8'))
            print("wrote text file " + self.emailFilepath)
            print("content: " + utf8_content)
            if len(commandToRun) > 0:
                print("running command: " + commandToRun)
                subprocess.call(commandToRun, shell=True)

    def getHtmlContent(self, parts):
        if not hasattr(self, 'html_content'):
            self.html_content = ''

            for part in parts:
                raw_content = part.get_payload(decode=True)
                charset = self.getPartCharset(part)
                self.html_content += raw_content.decode(charset, "replace")

            m = re.search('<body[^>]*>(.+)<\/body>', self.html_content, re.S | re.I)
            if (m != None):
                self.html_content = m.group(1)

        return self.html_content


    def createHtmlFile(self, parts, embed):
        utf8_content = self.getHtmlContent(parts)
        if len(utf8_content) < 1:
            return
        for img in embed:
            pattern = 'src=["\']cid:%s["\']' % (re.escape(img[0]))
            path = os.path.join(img[1])
            utf8_content = re.sub(pattern, 'src="%s"' % (path), utf8_content, 0, re.S | re.I)


        subject = self.getSubject()
        fromname = self.getFrom()[0]

        utf8_content = """<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="%s">
    <title>%s</title>
</head>
<body>
%s
</body>
</html>""" % (cgi.escape(fromname), cgi.escape(subject), utf8_content)

        if self.emailFilepath.endswith(' delete'):
            filetodelete = self.emailFilepath.replace(' delete', '')
            if os.path.isfile(filetodelete):
                os.remove(filetodelete)
                print('deleted file ' + filetodelete)
                return
        else:
            commandToRun = ''
            if utf8_content.startswith('imapsitesync-command: '):
                searchObj = re.search( r'^imapsitesync-command: (.*)\n(.*)', utf8_content, re.I)
                commandToRun = searchObj.group(1)
                commandToRun = commandToRun.strip()
                emailWithoutCommandLine = utf8_content.replace('imapsitesync-command: ' + commandToRun,'',1)
                if len(commandToRun) > 0:
                    utf8_content = emailWithoutCommandLine
            if not os.path.exists(os.path.dirname(self.emailFilepath)):
                os.makedirs(os.path.dirname(self.emailFilepath))
                print('created path ' + os.path.dirname(self.emailFilepath))
            if self.emailFilepath.endswith('/'):
                return
            with open(os.path.join(self.emailFilepath), 'wb') as fp:
                fp.write(bytearray(utf8_content, 'utf-8'))
            print('wrote html file ' + self.emailFilepath)
            if len(commandToRun) > 0:
                print("running command: " + commandToRun)
                subprocess.call(commandToRun.strip(), shell=True)

    def sanitizeFilename(self, filename):
        keepcharacters = (' ','.','_','-')
        return "".join(c for c in filename if c.isalnum() or c in keepcharacters).rstrip()


    def getParts(self):
        if not hasattr(self, 'message_parts'):
            counter = 1
            message_parts = {
                'text': [],
                'html': [],
                'embed_images': [],
                'files': []
            }

            for part in self.msg.walk():
                # multipart/* are just containers
                if part.get_content_maintype() == 'multipart':
                    continue

                # Applications should really sanitize the given filename so that an
                # email message can't be used to overwrite important files
                filename = part.get_filename()
                if not filename:
                    if part.get_content_type() == 'text/plain':
                        message_parts['text'].append(part)
                        continue

                    if part.get_content_type() == 'text/html':
                        message_parts['html'].append(part)
                        continue

                    ext = mimetypes.guess_extension(part.get_content_type())
                    if not ext:
                        # Use a generic bag-of-bits extension
                        ext = '.bin'
                    filename = 'part-%03d%s' % (counter, ext)

                filename = self.sanitizeFilename(filename)

                content_id =part.get('Content-Id')
                if (content_id):
                    content_id = content_id[1:][:-1]
                    message_parts['embed_images'].append((content_id, filename))

                counter += 1
                message_parts['files'].append((part, filename))
            self.message_parts = message_parts
        return self.message_parts


    def saveEmailPartsAsFiles(self):
        message_parts = self.getParts()

        if message_parts['text'] and not message_parts['html']:
            self.createTextFile(message_parts['text'])

        if message_parts['html']:
            self.createHtmlFile(message_parts['html'], message_parts['embed_images'])

        if message_parts['files']:
            emailFolderpath = os.path.dirname(self.emailFilepath)
            for afile in message_parts['files']:

                if self.emailFilepath.endswith(' delete'):
                    filetodelete = os.path.join(emailFolderpath, afile[1])
                    if os.path.isfile(filetodelete):
                        os.remove(filetodelete)
                        print('deleted file attachment ' + filetodelete)
                else:
                    with open(os.path.join(emailFolderpath, afile[1]), 'wb') as fp:
                        payload = afile[0].get_payload(decode=True)
                        if payload:
                            fp.write(payload)
                            print('wrote attachment ' + os.path.join(emailFolderpath, afile[1]))

